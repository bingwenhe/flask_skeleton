# Random Number Generator

This project contains two services:

_Service I (Frontend)_:

Web service that takes a number from the user. Call Service II to generate a random number and return whether it’s a prime or not.

_Service II (Backend)_:

Input: Number

Output: Return an pseudo-random number using the middle-square algorithm

## How to start the project
`docker-compose up -d --build`

Now you have two docker containers running.

- Backend, running on 0.0.0.0:5000
- Frontend, running on 0.0.0.0:80

Open `0.0.0.0:80/start_page.html`. A webpage should be shown.

Input seed number into `Input field`, then click on the button `Generate random number`.

## How to run tests
Stand in folder `random_number_generator` and execute `pytest --cov-report term-missing --cov-config .coveragerc --cov .`
Test coverage should be 100%. Some tests have been omitted, as can be seen in `.coveragerc`.

## Design

#### Frontend
Just some plain HTML, and some simple JavaScript.

#### Backend
1. Swagger

   For api specification / verification. HTML can be seen at http://0.0.0.0:5000/ui

2. Middle square algorithm

   If the length of the input number is odd, the last digit of the newly-generated number will be removed first, then remove evenly from both start and end if needed.

3. Is it a prime?

   Not being a mathematician, I have done my research on the Internet to find the best algorithm for this. The algo I found seems not very practical for this project.
   Efficiency is more important in this case, so I devised a method myself:

   A list of prime numbers will be PREDETERMINED, and read in as `PRIME_LIST` when the backend code starts, then when a `target_number` comes in, following steps are performed:
   - If `target_number` is in `PRIME_LIST`, it is a prime number
   - If `target_number` is less than the last number in `PRIME_LIST`, it can not be a prime number
   - If `target_number` is larger than the last number in `PRIME_LIST`, the square root of `target_number`will be calculated as `middle_point`
     - Expand the `PRIME_LIST` to `middle_point`, if necessary.
     - Save into `PRIME_LIST` (**NOTE**: this step is _NOT_ implemented yet)
     - Check against every number in `PRIME_LIST` to see if `target_number` can be divided. If cannot, then `target_number` is a prime, otherwise not.

#### Docker
Both backend and frontend are dockerized.

#### Test

- Frontend: Selenium could have been used here for test, but not implemented
- Backend: PyTest is used to perform unit-test.
