import logging
import os
import exceptions
logging.basicConfig(level=logging.INFO)
LOGGER = logging.getLogger(__name__)

PRIME_LIST_FILE = os.path.join(os.path.dirname(__file__), "prime_list")


def read_prime_list_file():
    with open(PRIME_LIST_FILE) as prime_file:
        line = prime_file.readline()
        return list(map(int, line.split(',')))


PRIME_LIST = read_prime_list_file()


# Here we deliberately use a mutable default value (a list) for the
# argument generated_numbers to store generated random numbers
def get_random_number(input_number, generated_numbers=[]):

    LOGGER.info(f"Input number is: {input_number}")

    loop_result = []  # This array will hold all the random numbers generated within this loop

    while True:
        random_number = middle_square_method(input_number)

        if random_number not in generated_numbers:  # A unique random number!
            generated_numbers.append(random_number)
            message = {
                "random_number": random_number,
                "is_prime": is_prime(random_number)
            }
            return message

        if random_number in loop_result:
            LOGGER.info("No more random numbers can be generated")
            raise exceptions.RandomNumberExhausted()

        loop_result.append(random_number)
        input_number = random_number


# Invented by the almighty John von Neumann
def middle_square_method(input_number):
    input_number_length = len(str(input_number))
    new_number_string = str(input_number * input_number).zfill(2 * input_number_length)

    digits_to_trim = int(input_number_length / 2)

    if input_number_length % 2 != 0:
        # If the length of the input number is odd, the last digit of the newly-generated number will be trimmed first
        new_number_string = new_number_string[:-1]

    if digits_to_trim == 0:
        return int(new_number_string)
    else:
        return int(new_number_string[digits_to_trim:-digits_to_trim])


def expand_prime_list(end_number):
    last = PRIME_LIST[-1]
    for n in range(last, end_number + 1):
        n_is_prime = True
        for p in PRIME_LIST:
            if n % p == 0:
                n_is_prime = False
                break
        if n_is_prime is True:
            PRIME_LIST.append(n)


def is_prime(target_number):
    # target number should be a natural number that is larger than 1
    if type(target_number) is not int or target_number < 2:
        return False

    # It is a prime if it's already in PRIME_LIST
    if target_number in PRIME_LIST:
        return True

    # It is not a prime if it's smaller than the last prime in PRIME_LIST
    if target_number < PRIME_LIST[-1]:
        return False

    middle_point = int(target_number ** 0.5)
    expand_prime_list(middle_point)

    LOGGER.info(f"Final prime list: \n {PRIME_LIST}")

    for prime in PRIME_LIST:
        if target_number % prime == 0:
            return False

    return True
