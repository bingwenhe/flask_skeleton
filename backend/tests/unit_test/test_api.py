import sys
import os
new_path = os.path.join(os.path.dirname(__file__), "..")
new_path = os.path.join(new_path, '..')
sys.path.append(new_path)
print(sys.path)
import pytest
import api
import exceptions


def sub_test_get_random_number_multi(input_number, random_number, is_prime, exception):
    if exception is None:
        result = api.get_random_number(input_number)
        assert result["random_number"] == random_number
        assert result["is_prime"] == is_prime
    else:
        with pytest.raises(exception):
            result = api.get_random_number(input_number)
            assert result["random_number"] == random_number
            assert result["is_prime"] == is_prime


@pytest.mark.parametrize("input_number, random_number, is_prime, exception", [
    (12, 14, False, None),
    (12, 19, True, None),
    (12, 36, False, None),
    (12, 29, True, None),
    (12, 84, False, None),
    (12, 5, True, None),
    (12, 2, True, None),
    (12, 0, False, None),
    (12, None, None, exceptions.RandomNumberExhausted),
])
def test_get_random_number_00(input_number, random_number, is_prime, exception):
    sub_test_get_random_number_multi(input_number, random_number, is_prime, exception)


# Loop situation. 60 will always generate 60 with our algorithm
@pytest.mark.parametrize("input_number, random_number, is_prime, exception", [
    (47, 20, False, None),
    (47, 40, False, None),
    (47, 60, False, None),
    (47, None, None, exceptions.RandomNumberExhausted),
])
def test_get_random_number_01(input_number, random_number, is_prime, exception):
    sub_test_get_random_number_multi(input_number, random_number, is_prime, exception)


# Loop situation. 24 will generate 57 with our algorithm, and 57 -> 24
@pytest.mark.parametrize("input_number, random_number, is_prime, exception", [
    (24, 57, False, None),
    (24, 24, False, None),
    (24, None, None, exceptions.RandomNumberExhausted),
])
def test_get_random_number_01(input_number, random_number, is_prime, exception):
    sub_test_get_random_number_multi(input_number, random_number, is_prime, exception)


@pytest.mark.parametrize("test_number, result", [
    (1, 0),
    (7, 4),
    (12, 14),
    (14, 19),
    (19, 36),
    (32, 2),
    (100, 100),
    (317, 4),
])
def test_middle_square_method(test_number, result):
    assert api.middle_square_method(test_number) == result


@pytest.mark.parametrize("test_number, result", [
    (0, False),    # Special case, 0
    (1, False),    # Special case, 1
    (2, True),     # Special case, 2
    (2.5, False),  # Special case, non-int

    # Require prime_list expansion

    # Inclusive cases
    (169, False),  # square(13)
    (170, False),
    (288, False),
    (289, False),  # square(17), this one will require prime list expansion
    (290, False),

    # Performance test
    (1982119441, False),

    # Following are random cases
    (3, True),
    (4, False),
    (5, True),
    (6, False),
    (10, False),
    (11, True),
    (16, False),
    (59, True),
    (40000, False),
    (50053, True),
    (2500, False),
    (2501, False),
    (887, True),
    (2503, True),
])
def test_is_prime(test_number, result):
    api.PRIME_LIST = [2, 3, 5, 7, 11, 13]
    assert api.is_prime(test_number) == result

@pytest.mark.parametrize("test_number, result", [
    (50, [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47]),
    (13, [2, 3, 5, 7, 11, 13]),
    (14, [2, 3, 5, 7, 11, 13]),
    (17, [2, 3, 5, 7, 11, 13, 17]),
])
def test_expand_prime_list(test_number, result):
    api.PRIME_LIST = [2, 3, 5, 7, 11, 13]
    api.expand_prime_list(test_number)
    assert api.PRIME_LIST == result
