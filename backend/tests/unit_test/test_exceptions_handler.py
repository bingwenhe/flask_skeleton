import sys
import os
new_path = os.path.join(os.path.dirname(__file__), "..")
new_path = os.path.join(new_path, '..')
sys.path.append(new_path)
print(sys.path)
import backend
import exceptions_handler
import exceptions
import json


def test_handle_random_number_exhausted_exception():
    with backend.app.create_app().app_context():
        e = exceptions.RandomNumberExhausted()
        response_json = json.loads(exceptions_handler.handle_random_number_exhausted_exception(e).data)
        assert not response_json['is_prime']
        assert not response_json['random_number']
